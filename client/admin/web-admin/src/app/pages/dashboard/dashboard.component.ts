import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  displayedColumns: string[] = ['Name', 'Last Version'];
  dataSource = [];
  showFiller = false;
  public userForm: any;
  constructor() { }

  ngOnInit(): void {
    this.userForm = new FormGroup({
      id: new FormControl(''),
      name: new FormControl(''),
      version: new FormControl(''),
      OSType: new FormControl(''),
      minimumRequiredVersion: new FormControl(''),
    });
    fetch("http://localhost:3000/getAllApplications").then(resp =>{
      if(resp.status === 200)
      resp.json().then(resss => {
        console.log(resss.application)
        this.dataSource = resss.application;
      })
      else console.error(resp.status)
    }).catch( err => {
      console.error(err.status)
    }
    );
  }

  openDialog(): void {
}

  addApplication() {
    let app = {
      applicationID: this.userForm.value.id,
      applicationName: this.userForm.value.name,
      lastVersion: {
        version: this.userForm.value.version,
        OSTypes: {
          OSType:  this.userForm.value.OSType,
          minimumRequiredVersion:  this.userForm.value.minimumRequiredVersion
        }
      }
    }
    fetch("http://localhost:3000/createApplication", {
      method: 'POST',
      headers: {
        'content-type': 'application/json;charset=UTF-8',
      },
      body: JSON.stringify(app)
    })
    window.location.reload();
  }

}
