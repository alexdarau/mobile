import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  public userForm: any;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.userForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl(''),
    });
  }

  login() {
    let user = {
      "email": this.userForm.value.email,
      "password": this.userForm.value.password,
      "device":
      {
        "deviceId": "11"
      }
    }
    fetch("http://localhost:3000/auth/login", {
      method: 'POST',
      headers: {
        'content-type': 'application/json;charset=UTF-8',
      },
      body: JSON.stringify(user)
    }).then(resp =>{
      if(resp.status === 200)
        this.router.navigate(["/dashboard"])
      else console.error(resp.status)
    }).catch( err => {
      console.error(err.status)
    }
    )
  }

  goToRegister() {
    this.router.navigate(["/register"])
  }

}
