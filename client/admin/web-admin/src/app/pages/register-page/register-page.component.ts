import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {

  public userForm:any;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.userForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl(''),
      confirmPassword: new FormControl(''),
    });
  }

  register(){
    let user = {
      "email": this.userForm.value.email,
      "password": this.userForm.value.password,
      "currentDeviceID": 4
    }
    fetch("http://localhost:3000/auth/register", {
      method: 'POST',
      headers: {
        'content-type': 'application/json;charset=UTF-8',
      },
      body: JSON.stringify(user)
    }).then(resp =>{
      if(resp.status === 200)
        this.router.navigate(["/dashboard"])
      else console.error(resp.status)
    }).catch( err => {
      console.error(err.status)
    }
    )
  }

}
