require("dotenv").config();
var express = require("express");
const mongoose = require("mongoose");
const app = express();
var registerRoutes = require("./src/routes/registerOAuth.js");
var applicationRoutes = require("./src/routes/application/createApplication.js");
var deviceRoutes = require("./src/routes/device/createDevice.js");
var deleteApp = require("./src/routes/application/deleteApplication.js");
var getAllApplications = require("./src/routes/application/getAllApplications.js");
var getAllUsers = require("./src/routes/users/getUsers.js");


console.log(process.env.MONGO_DB_CONN_STRING)
mongoose.connect(
    process.env.MONGO_DB_CONN_STRING,
    {
      dbName: "it_marathon",
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
    (e) => {
      console.log("db connection", !e ? "successfull" : e);
    }
  );

app.get('/', (req, res) => {
    res.send("HTTP GET!")
});

app.use(registerRoutes);
app.use(applicationRoutes);
app.use(deviceRoutes);
app.use(deleteApp);
app.use(getAllApplications);
app.use(getAllUsers);


app.listen(process.env.PORT, () => {
    console.log(`Express a listening port ${process.env.PORT}`)
});