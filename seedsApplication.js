const mongoose = require('mongoose');
const Applications = require("./src/models/application");
const { faker } = require('@faker-js/faker');

mongoose.connect('mongodb+srv://loregostian:ngkp5QIfA2W6li92@cluster0.7djem.mongodb.net/test?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then( () => {
        console.log('Mongo Connection OPEN!');
    })
    .catch( (err) => {
        console.log(err);
    })

const seedDB = async () => {
    let apps = [];
    let osty = ['Android', 'iOS', 'Linux', 'Windows'];

    for (let i=0; i <50; i++) {
        apps.push({
            applicationID: faker.random.word(),
            applicationName: faker.random.word(), 
            
            lastVersion: [{
                version: faker.datatype.number({
                    'min': 0,
                    'max': 5
                }),

                OSTypes: [{
                    OSType: osty[Math.floor(Math.random() * osty.length)],
                    minimumRequiredVersion: faker.datatype.number({
                        'min': 8,
                        'max': 12
                    })
                }]
            }],

            applicationVersions: faker.random.arrayElements()
        })
    } 

    console.log(apps);

    await Applications.deleteMany({});
    await Applications.insertMany(apps);
};

seedDB().then( () => {
    mongoose.connection.close();
})
