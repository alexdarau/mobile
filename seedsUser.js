const mongoose = require('mongoose');
const Users = require("./src/models/user");
const { faker } = require('@faker-js/faker');

mongoose.connect('mongodb+srv://loregostian:ngkp5QIfA2W6li92@cluster0.7djem.mongodb.net/test?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then( () => {
        console.log('Mongo Connection OPEN!');
    })
    .catch( (err) => {
        console.log(err);
    })

const seedDB = async () => {
    let user = [];

    for (let i=0; i <50; i++) {
        user.push({
            email: faker.internet.email(),
            password: faker.internet.password()
        })
    } 

    console.log(user);

    await Users.deleteMany({});
    await Users.insertMany(user);
};

seedDB().then( () => {
    mongoose.connection.close();
})
