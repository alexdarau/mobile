const mongoose = require("mongoose");

const applicationsSchema = mongoose.Schema({ 
  applicationID: {
      type: String,
      require: true,
  },

  applicationName: {
      type: String,
      require: true,
  },

  lastVersion: [{
        version: Number,
        OSTypes: [{
            OSType:  {
                type: String,
                enum: ['Android', 'iOS', 'Linux', 'Windows'],
                default: 'Android',
                require: true,
            },

            minimumRequiredVersion: {
                type: Number,
                require: true,
            },
        }]
  }],

  applicationVersions: {
      type: Array,
      require: true,
  }

});

applicationsSchema.index({ id: 1, applicationID: 1 });

const Applications = mongoose.model("applications", applicationsSchema);

module.exports = Applications;