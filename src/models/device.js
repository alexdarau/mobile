const mongoose = require("mongoose");

const devicesSchema = mongoose.Schema({ 
  deviceID: {
      type: String,
      require: true,
  },

  OSType: {
      type: String,
      enum: ['Android', 'iOS', 'Linux', 'Windows'],
      default: 'Android',
      require: true,
  },

  OSVersion: {
      type: Number,
      require: true,
  },

  application: [{
        appID: String,
        version: mongoose.Schema.Types.Mixed
  }]

});

devicesSchema.index({ id: 1, deviceID: 1 });

const Devices = mongoose.model("devices", devicesSchema);

module.exports = Devices;