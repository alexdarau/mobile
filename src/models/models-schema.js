//User
user: {
    _id: 'mongoDBId';
    email: 'bla@email.ro';
    password: "password";
    devices: ["deviceID1", "deviceID2"];
    role: "client"; //enum["client", "admin"];
    currentDeviceID: "deviceID1";
}

//Device
device: {
    _id: "mongoDBId";
    deviceID: "deviceID1";
    OSType: "Android";
    OSVersion: 9;
    application: [
        {
            id: "appID1",
            version: 3,
        }, 
        { 
            id: "appID2",
            version: 1,
        }
    ];
}

//Application
application: {
    _id: "mongoDB";
    applicationID: "appID1";
    applicationName: "My App Name";
    lastVersion: {
        version: 4;
        OSTypes: [
            {
                OSType: "Android",
                minimumRequiredVersion: 10,
            }
        ];
    }
    applicationVersions: [1,2,3,4];
}