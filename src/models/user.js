const mongoose = require("mongoose");

const usersSchema = mongoose.Schema({ 
  email: {
      type: String,
      require: true,
  },

  password: {
      type: String,
      require: true,
  },

  devices: {
      type: Array,
      require: true,
  },

  role: {
        type: String,
        enum: ['Client', 'Admin'],
        default: 'Client',
        require: true,
  },

  currentDeviceID: {
      type: String,
      require: true,
  }
  
});

usersSchema.index({ userId: 1, email: 1 });

const Users = mongoose.model("users", usersSchema);

module.exports = Users;