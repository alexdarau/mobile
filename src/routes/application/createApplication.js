var express = require("express");
var jwt = require("jsonwebtoken");
const Application = require("../../models/application.js");

const server = express();

server.use(express.json());

server.post('/createApplication',
    async (req, res) => {

        console.log(req.body)
        const { applicationID, applicationName, lastVersion, applicationVersions } = req.body;

        const application = {
            applicationID,
            applicationName,
            lastVersion,
            applicationVersions
        }

        const applicationReq = await Application.findOne(application);

        if (!applicationReq) {
            await Application.create({
                ...application
            }).then(application =>
                res.status(200).json({
                    message: "Application successfully created",
                    application,
                })
                // res.redirect(`/api/users/${user.id}`);
            )
        } else {
            res.status(401).json({
                message: "Application not created",
            });
            // res.redirect(constants.UNAUTHORIZED_URL);
        }
    });

function verifyToken(req, res, next) {
    //Auth header value = > send token into header



    //check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {

        //split the space at the bearer
        const bearer = bearerHeader.split(' ');
        //Get token from string
        const bearerToken = bearer[1];
        console.log("🚀 ~ file: createApplication.js ~ line 55 ~ verifyToken ~ bearerHeader", bearerToken)
        //set the token
        req.token = bearerToken;

        //next middleweare
        next();

    } else {
        //Fobidden
        res.sendStatus(403);
    }

}

module.exports = server;