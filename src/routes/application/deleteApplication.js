const { application } = require("express");
var express = require("express");
const Application = require("../../models/application.js");

const server = express();
server.use(express.json());

server.delete('/:applicationID', 
    async (req, res) => {
        const application = 
            await Application.findOneAndDelete({
                applicationID: req.params.applicationID
            }).then(application =>
                res.status(200).json({
                message: "Application successfully deleted",
                application,
                })
            )
    }
);

module.exports = server;