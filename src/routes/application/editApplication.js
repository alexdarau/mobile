var express = require("express");
const Application = require("../../models/application.js");

const server = express();

server.use(express.json());

server.post('/editApplication',
  async (req, res) =>{
    const { applicationID, newVersion, OSTypes } = req.body;
    const applicationReq = await Application.findOne({applicationID: applicationID});

    if(!applicationReq) {
        res.status(404).json({
            message: "Not found",
          });
    } else {
        await Application.findOneAndUpdate(
            {applicationID: applicationID}, 
            { $set:{
                    lastVersion: {
                        version: newVersion,
                        OSTypes: OSTypes
                    }
                }
            }
        )
        res.status(200).json({
          message: "Application version was updated",
        });
    }
});

module.exports = server;