var express = require("express");
const Application = require("../../models/application.js");

const server = express();

server.use(express.json());

server.get('/getAllApplications',
  async (req, res) =>{
    const application =  
        await Application.find({})
            .then(application => 
                    res.status(200).json({
                        application
            })
        )
});

module.exports = server;