var express = require("express");
const Device = require("../../models/device.js");

const server = express();

server.use(express.json());

server.post('/createDevice',
  async (req, res) =>{
    const { deviceID, OSType, OSVersion, application } = req.body;
    const device = {
        deviceID,
        OSType,
        OSVersion,
        application
    }

    const deviceReq = await Device.findOne(device);

    if(!deviceReq) {
        await Device.create({
            ...device
        }).then(device =>
            res.status(200).json({
            message: "Device successfully created",
            application,
            })
            // res.redirect(`/api/users/${user.id}`);
        )
    } else {
        res.status(401).json({
          message: "Device not created",
        });
        // res.redirect(constants.UNAUTHORIZED_URL);
    }
});

module.exports = server;