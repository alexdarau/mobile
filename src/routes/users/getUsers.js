var express = require("express");
const Users = require("../../models/user.js");

const server = express();

server.use(express.json());

server.get('/getAllUsers',
  async (req, res) =>{
    const users =  
        await Users.find({})
            .then(users => 
                    res.status(200).json({
                        users
            })
        )
});

module.exports = server;